Quang Tran, 6339816

Build normally using Unity. All assets included in project.

'WASD' for movement.
'mouse movement' to look around.
'space' to activate jetpack and move up.
'left click' to shoot.
'scroll wheel' to change weapon (must pick up crates first).