﻿using UnityEngine;
using System.Collections;

public class BalloonFactory : MonoBehaviour 
{
	public static BalloonFactory instance;
	public GameObject balloon;
	public GameObject cluster;
	public GameObject hotAirBalloon;

	void Awake () 
	{
		if(instance != null)
		{
			Destroy (this.gameObject);
		}
		instance = this;
	}
	
	public GameObject SpawnBalloon()
	{
		return Instantiate(balloon) as GameObject;
	}

	public GameObject SpawnCluster()
	{
		return Instantiate(cluster) as GameObject;
	}

	public GameObject SpawnHotAirBalloon()
	{
		return Instantiate(hotAirBalloon) as GameObject;
	}
}
