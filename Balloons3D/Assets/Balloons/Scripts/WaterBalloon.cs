﻿using UnityEngine;
using System.Collections;

public class WaterBalloon : MonoBehaviour 
{
	public float lifetime;
	public float speed;

	private void Awake()
	{
		Destroy (this.gameObject, lifetime);
	}

	public void ShootAt(Vector3 target)
	{
		float r = UnityEngine.Random.value + UnityEngine.Random.value + UnityEngine.Random.value;
		r = (r/3 - 0.5f) * 3; // Random from -1.5 to 1.5 using a bell curve

		Vector3 direction = target - this.transform.position;
		Vector3 x = new Vector3(direction.x + r, 0, direction.z + r);
		float y = direction.y + r;

		// Give the horizontal direction
		rigidbody.velocity = speed * x.normalized;

		// Give the proper vertical speed (d = v0 * t + 0.5 * a * t^2)
		float t = x.magnitude/speed;
		float a = Physics.gravity.y/rigidbody.mass;
		float v0 = (y - (0.5f * a * t * t))/t;

		Vector3 v = rigidbody.velocity;
		v.y = v0;
		rigidbody.velocity = v;
	}

	public void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Player" || collider.tag == "Projectile")
		{
			Destroy (this.gameObject);
		}
	}
}
