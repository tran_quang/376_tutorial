﻿using UnityEngine;
using System.Collections;

public class HotAirBalloon : MonoBehaviour 
{
	public GameObject waterBalloon;
	public float shootCooldown;
	public float maxSpeed;

	private Transform player;
	private float shootTimer;

	private void Awake () 
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
		shootTimer = shootCooldown;

		float x = UnityEngine.Random.Range (-maxSpeed, maxSpeed);
		float y = UnityEngine.Random.Range (-maxSpeed, maxSpeed);
		float z = UnityEngine.Random.Range (-maxSpeed, maxSpeed);

		rigidbody.velocity = new Vector3(x, y, z);
	}
	
	private void Update () 
	{
		shootTimer -= Time.deltaTime;

		if(shootTimer < 0)
		{
			shootTimer = shootCooldown;
			GameObject wb = Instantiate(waterBalloon, this.transform.position, Quaternion.identity) as GameObject;
			wb.SendMessage("ShootAt", player.position);
		}
	}
}
