﻿using UnityEngine;
using System.Collections;

public class Balloon : MonoBehaviour 
{
	public float floatSpeed;

	private LineRenderer lineRenderer;

	void Awake () 
	{
		lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.SetColors(Color.white, Color.white);
		lineRenderer.SetWidth(0.05f, 0.05f);
		lineRenderer.SetPosition (1, Vector3.zero);
	}
	
	void Update () 
	{
		Quaternion inverse = Quaternion.Inverse(this.transform.localRotation);
		lineRenderer.SetPosition(0, inverse * -this.transform.localPosition);
	}

	public void ChangeCluster(GameObject newCluster)
	{
		GetComponent<HingeJoint>().connectedBody = newCluster.rigidbody;
		transform.parent = newCluster.transform;
	}

	public void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Projectile")
		{
			SendMessageUpwards("OnHit");
		}
	}
}
