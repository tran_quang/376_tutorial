﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cluster : MonoBehaviour 
{
	public List<Balloon> balloons = new List<Balloon>();
	public float maxSpeed;
	public float windForce;

	private void Awake () 
	{
		float y = UnityEngine.Random.Range (0.0f, maxSpeed);
		rigidbody.velocity = new Vector3(0, y, 0);
	}

	private void Update()
	{
		// Get perlin noise from -1 to 1
		float x = (Mathf.PerlinNoise(Time.time, 0) - 0.5f) * 2;
		float z = (Mathf.PerlinNoise(0, Time.time) - 0.5f) * 2;

		rigidbody.AddForce(windForce * new Vector3(x, 0, z) * Time.deltaTime);

		if(rigidbody.velocity.magnitude > maxSpeed)
		{
			rigidbody.velocity = 0.8f*rigidbody.velocity;
		}
	}

	public void AddBalloon(Balloon balloon)
	{
		balloons.Add(balloon);
	}

	public void OnHit()
	{
		if(balloons.Count > 1)
		{
			Split();
			GameController.instance.score += 1;
		}
		else
		{
			if(UnityEngine.Random.value < 0.2f)
			{
				CrateFactory.instance.SpawnCrate(this.transform.position);
			}
			GameController.instance.PopBalloon();
			GameController.instance.score += 2;
			Destroy (this.gameObject);
		}
	}

	private void Split()
	{
		GameObject newObject = BalloonFactory.instance.SpawnCluster();
		Cluster newCluster = newObject.GetComponent<Cluster>();
		newCluster.transform.position = this.transform.position;

		// Split at least 1 balloon
		balloons[0].ChangeCluster(newObject);
		newCluster.AddBalloon(balloons[0]);

		for(int i = 1; i < balloons.Count - 1; i++)
		{
			if(UnityEngine.Random.value > 0.5f)
			{
				balloons[i].ChangeCluster(newObject);
				newCluster.AddBalloon(balloons[i]);
			}
		}

		balloons.RemoveAll((Balloon b) => {return newCluster.balloons.Contains (b);});
	}
}
