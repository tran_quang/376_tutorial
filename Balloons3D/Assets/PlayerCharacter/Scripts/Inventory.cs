﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour 
{
	public List<GameObject> guns;
	private List<Gun> guns_component = new List<Gun>();

	private Transform head;
	public int gunIndex = 1;

	private void Awake()
	{
		head = this.transform.FindChild("Head");
		for(int i = 0; i < guns.Count; i++)
		{
			GameObject gun = Instantiate(guns[i]) as GameObject;
			gun.transform.parent = head;
			gun.transform.localPosition = new Vector3(0.5f, -0.3f, 0.75f);

			guns[i] = gun;
			guns_component.Add(gun.GetComponent<Gun>());
			if(i != gunIndex) // The first value of gunIndex is the starter gun
			{
				gun.SetActive(false);
			}
		}
	}

	private void Update()
	{
		float scroll = Input.GetAxis ("Mouse ScrollWheel");
		if(scroll != 0)
		{
			SwitchGun(scroll > 0);
		}
	}

	private void OnDisable()
	{
		guns[gunIndex].SetActive(false);
	}

	private void OnEnable()
	{
		guns[gunIndex].SetActive(true);
	}

	public void AddGun()
	{
		int r = UnityEngine.Random.Range(0, guns.Count);
		guns_component[r].AddAmmo();
	}

	public void SwitchGun(bool forward)
	{
		int adder = 1;
		if(!forward)
		{
			adder = -1;
		}

		guns[gunIndex].SetActive(false);
		Quaternion currentRotation = guns[gunIndex].transform.rotation;
		bool switchedGun = false;
		do
		{
			gunIndex = (gunIndex + adder + guns.Count) % guns.Count; // Add guns.count in case adding goes to -1
			if(guns_component[gunIndex].ammo > 0)
			{
				guns[gunIndex].SetActive (true);
				guns[gunIndex].transform.rotation = currentRotation;
				switchedGun = true;
			}
		}while(!switchedGun);
	}
}
