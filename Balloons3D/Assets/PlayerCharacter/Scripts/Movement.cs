﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour 
{
	public float walkSpeed;
	private GameObject radar;

	private void Awake()
	{
		radar = transform.FindChild ("Head").FindChild("Radar").gameObject;
	}

	private void Update()
	{
		float vertical = Input.GetAxis ("Vertical");
		float horizontal = Input.GetAxis("Horizontal");

		rigidbody.AddForce(transform.forward * vertical * walkSpeed * Time.deltaTime);
		rigidbody.AddForce(transform.right * horizontal * walkSpeed * Time.deltaTime);

		if(Input.GetButtonDown("ToggleRadar"))
		{
			radar.SetActive (!radar.activeSelf);
		}
	}
}
