﻿using UnityEngine;
using System.Collections;

public class MouseLook : MonoBehaviour 
{
	public bool yAxis;
	public bool xAxis;
	public float rotationSpeed;

	void Update () 
	{
		if(yAxis)	
		{
			float y = Input.GetAxis("Mouse Y");
			Vector3 rotationAngles = transform.rotation.eulerAngles;
			if(rotationAngles.x > 270)
			{
				rotationAngles.x = Mathf.Clamp (rotationAngles.x - (y * rotationSpeed * Time.deltaTime), 280, 360);
			}
			else
			{
				rotationAngles.x = Mathf.Clamp (rotationAngles.x - (y * rotationSpeed * Time.deltaTime), -1, 80);
			}

			transform.rotation = Quaternion.Euler(rotationAngles);
		}

		if(xAxis)
		{
			float x = Input.GetAxis("Mouse X");
			Vector3 rotationAngles = transform.rotation.eulerAngles;
			rotationAngles.y += x * rotationSpeed * Time.deltaTime;
			
			transform.rotation = Quaternion.Euler(rotationAngles);
		}
	}
}
