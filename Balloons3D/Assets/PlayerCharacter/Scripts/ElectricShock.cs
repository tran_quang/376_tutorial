﻿using UnityEngine;
using System.Collections;

public class ElectricShock : MonoBehaviour 
{
	private AudioSource source;
	public float playTime;

	void Awake () 
	{
		source = GetComponent<AudioSource>();
	}

	void OnEnable()
	{
		StopAllCoroutines();
		StartCoroutine(StopSound ());
	}

	private IEnumerator StopSound () 
	{
		yield return new WaitForSeconds(playTime);
		source.Stop ();
	}
}
