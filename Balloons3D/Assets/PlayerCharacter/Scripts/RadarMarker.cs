﻿using UnityEngine;
using System.Collections;

public class RadarMarker : MonoBehaviour {

	private LineRenderer lineRenderer;

	void Awake () 
	{
		lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.SetPosition(0, Vector3.zero);
	}
	
	void Update () 
	{
		lineRenderer.SetPosition (1, new Vector3(0, -transform.localPosition.y/transform.localScale.y, 0));
	}
}
