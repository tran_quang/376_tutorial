﻿using UnityEngine;
using System.Collections;

public class PlayerLife : Life 
{
	public Texture2D icon;

	private GameObject malfunctionIndicator;
	private JetPack jetPack;
	private Bounce bounce;
	private Inventory inven;

	protected virtual void Awake()
	{
		malfunctionIndicator = transform.Find("Head/ErekiSmoke").gameObject;
		malfunctionIndicator.SetActive(false);

		jetPack = GetComponent<JetPack>();
		bounce = GetComponent<Bounce>();
		inven = GetComponent<Inventory>();
	}

	private void OnCollisionEnter(Collision c)
	{
		base.OnTriggerEnter(c.collider);
	}

	protected override IEnumerator TriggerInvincibility()
	{
		invincible = true;

		yield return StartCoroutine(FallDown());

		float waitFor = 0.2f;
		float timer = invincibilityTimer;
		bool visible = true;
		while(timer > 0)
		{
			visible = !visible;
			timer -= waitFor;
			yield return new WaitForSeconds(waitFor);
			ToggleAllRenderers(this.transform, visible);
		}
		
		ToggleAllRenderers(this.transform, true);
		invincible = false;
	}

	private IEnumerator FallDown()
	{
		float drag = rigidbody.drag;
		rigidbody.drag = 0;
		ToggleComponents();

		while(-GameController.instance.worldLimits.y < this.transform.position.y)
		{
			yield return null;
		}

		this.transform.position = Vector3.zero;
		this.transform.rotation = Quaternion.identity;

		ToggleComponents();
		rigidbody.drag = drag;
	}

	protected override void Die ()
	{
		rigidbody.drag = 0;
		ToggleComponents();

		GameObject g = Instantiate (deathSystem) as GameObject;
		g.transform.position = transform.Find ("Head/ErekiSmoke").position;
		g.transform.parent = transform.Find ("Head/ErekiSmoke").parent;

		GameController.instance.EndGame ();
	}

	private void ToggleComponents()
	{
		malfunctionIndicator.SetActive(!malfunctionIndicator.activeSelf);
		jetPack.enabled = !jetPack.enabled;
		collider.enabled = !collider.enabled;
		bounce.enabled = !bounce.enabled;
		inven.enabled = !inven.enabled;
	}
}
