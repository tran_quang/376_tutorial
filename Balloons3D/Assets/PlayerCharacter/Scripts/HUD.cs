﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HUD : MonoBehaviour 
{
	private Gun[] availableGuns;
	private PlayerLife life;

	private void Start()
	{
		List<GameObject> guns = GetComponent<Inventory>().guns;
		availableGuns = new Gun[guns.Count];

		for(int i = 0; i < availableGuns.Length; i++)
		{
			availableGuns[i] = guns[i].GetComponent<Gun>();
		}

		life = GetComponent<PlayerLife>();
	}

	private void OnGUI()
	{
		RenderInventory();
		RenderHealth ();
		RenderScore ();
	}

	private void RenderInventory()
	{
		float width = Screen.width/12;
		float height = Screen.height/10;
		
		float x = 3;
		float y = Screen.height - height - 3;
		
		for(int i = 0; i < availableGuns.Length; i++)
		{
			Color c = Color.white;
			if(availableGuns[i].ammo < 1)
			{
				c.a = 0.25f;
			}
			GUI.color = c;
			
			GUI.DrawTexture(new Rect(x, y, width, height), availableGuns[i].hudIcon);
			x += width + 3;
		}
	}

	private void RenderHealth()
	{
		float x = 3;
		float y = 3;

		float height = Screen.height/15;
		float width = Screen.height/15;
		
		GUI.color = Color.white;
		for(int i = 0; i < life.lifeRemaining; i++)
		{
			GUI.DrawTexture (new Rect(x, y, width, height), life.icon);
			x+= width + 3;
		}
	}

	private void RenderScore()
	{
		GUIStyle style = new GUIStyle();
		string score = GameController.instance.score.ToString();

		style.normal.textColor = Color.black;
		style.fontSize = 36;
		style.alignment = TextAnchor.UpperCenter;
		GUI.Label (new Rect(Screen.width/2, -1, 0, 0), score, style);
		GUI.Label (new Rect(Screen.width/2, 1, 0, 0), score, style);
		GUI.Label (new Rect(Screen.width/2 - 1, 0, 0, 0), score, style);
		GUI.Label (new Rect(Screen.width/2 + 1, 0, 0, 0), score, style);
		GUI.Label (new Rect(Screen.width/2 + 1, -1, 0, 0), score, style);
		GUI.Label (new Rect(Screen.width/2 + 1, 1, 0, 0), score, style);
		GUI.Label (new Rect(Screen.width/2 - 1, -1, 0, 0), score, style);
		GUI.Label (new Rect(Screen.width/2 - 1, 1, 0, 0), score, style);

		style.normal.textColor = Color.white;
		style.fontSize = 36;
		style.alignment = TextAnchor.UpperCenter;
		GUI.Label (new Rect(Screen.width/2, 0, 0, 0), score, style);
	}
}
