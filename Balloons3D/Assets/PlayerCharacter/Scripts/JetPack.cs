﻿using UnityEngine;
using System.Collections;

public class JetPack : MonoBehaviour 
{
	public float propulsionForce;

	private void Update () 
	{
		if(Input.GetButton ("Jump"))	
		{
			rigidbody.AddForce(Vector3.up * propulsionForce * Time.deltaTime);
		}
		else
		{
			rigidbody.AddForce(Vector3.down * propulsionForce * Time.deltaTime);
		}
	}
}
