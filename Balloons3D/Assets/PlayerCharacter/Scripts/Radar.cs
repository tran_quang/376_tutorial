﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Radar : MonoBehaviour 
{
	public GameObject marker;

	private Transform center;
	private float radius;
	private float radarSize;
	private Dictionary<Collider, GameObject> pointers = new Dictionary<Collider, GameObject>();

	private void Start()
	{
		center = transform.parent;
		radius = GameController.instance.worldLimits.x * 2;
		radarSize = transform.localScale.x/2;
		this.gameObject.SetActive(false);
	}

	private void Update () 
	{
		List<Collider> objects = Physics.OverlapSphere(center.position, radius).ToList ();
		objects.RemoveAll((c) => {return c.isTrigger;});
		for(int i = 0; i < objects.Count; i++)
		{
			GameObject pointer;
			if(!pointers.TryGetValue(objects[i], out pointer))
			{
				pointer = Instantiate(marker) as GameObject;
				pointer.transform.parent = this.transform;
				pointer.transform.rotation = this.transform.rotation;
				pointers.Add (objects[i], pointer);
			}

			pointer.transform.localPosition = (objects[i].transform.position - center.position).normalized * radarSize;
		}

		List<Collider> notFound = pointers.Keys.Except(objects).ToList ();
		for(int i = 0; i < notFound.Count; i++)
		{
			Destroy(pointers[notFound[i]]);
			pointers.Remove (notFound[i]);
		}

		this.transform.rotation = Quaternion.identity;
	}
}
