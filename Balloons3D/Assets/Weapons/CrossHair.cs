﻿using UnityEngine;
using System.Collections;

public class CrossHair : MonoBehaviour 
{
	public Texture2D crossHair;

	private Vector2 min;

	private void Awake()
	{
		min = new Vector2(Screen.width/2 - crossHair.width/2, Screen.height/2 - crossHair.height/2);
	}

	private void OnGUI () 
	{
		GUI.DrawTexture(new Rect(min.x, min.y, crossHair.width, crossHair.height), crossHair);
	}
}
