﻿using UnityEngine;
using System.Collections;

public class StandardGun : Gun 
{
	public GameObject bullet;
	public float delay = 0.2f;
	public float bulletSpeed;

	private float currentDelay = 0.0f;

	void Update () 
	{
		if(Input.GetButton("Fire1") && currentDelay < 0.0f)
		{
			currentDelay = delay;
			ammo--;

			GameObject b = Instantiate(bullet, this.transform.position, this.transform.rotation) as GameObject;
			b.rigidbody.velocity = transform.forward * bulletSpeed;
		}
		else
		{
			currentDelay -= Time.deltaTime;
		}
	}
}
