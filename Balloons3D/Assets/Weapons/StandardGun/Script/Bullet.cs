﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{
	public float lifetime;
	public GameObject explosion;
	public float explosionForce;

	void Awake()
	{
		Destroy (this.gameObject, lifetime);
	}

	void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Player" || collider.tag == "Weapon")
		{
			return;
		}
		else if(collider.tag == "Balloon")
		{
			Instantiate(explosion, this.transform.position, this.transform.rotation);
			collider.rigidbody.AddExplosionForce(explosionForce, this.transform.position, 1.0f);
			Destroy (this.gameObject);
		}
	}
}
