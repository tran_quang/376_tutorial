﻿using UnityEngine;
using System.Collections;

public abstract class Gun : MonoBehaviour 
{
	public Texture2D hudIcon;
	public string type;
	public int ammo;
	public int onPickup;

	public override bool Equals (object o)
	{
		if(o.GetType().Equals(this.GetType()))
		{
			Gun g = o as Gun;
			return this.type == g.type;
		}
		else
		{
			return false;
		}
	}

	public override int GetHashCode ()
	{
		return type.GetHashCode ();
	}

	public void AddAmmo()
	{
		ammo += onPickup;
		Debug.Log ("GOT " + onPickup + " AMMO FOR " + type);
	}
}
