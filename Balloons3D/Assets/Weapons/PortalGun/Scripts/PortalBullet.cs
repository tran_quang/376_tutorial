﻿using UnityEngine;
using System.Collections;

public class PortalBullet : MonoBehaviour 
{
	public float gravityForce;

	private float radius;
	private ParticleSystem ps;

	private void Awake()
	{
		radius = ((SphereCollider)collider).radius;
		collider.enabled = false;

		ps = transform.GetChild (0).particleSystem;
	}

	private void Update()
	{
		Collider[] c = Physics.OverlapSphere(this.transform.position, radius);
		for(int i = 0; i < c.Length; i++)
		{
			if(c[i].tag == "Balloon")
			{
				Vector3 direction = (this.transform.position - c[i].transform.position).normalized;
				c[i].rigidbody.AddForce(direction * gravityForce * Time.deltaTime);
			}
		}

		if(!ps.IsAlive(false))
		{
			collider.enabled = true;
		}
	}
}
