﻿using UnityEngine;
using System.Collections;

public class PortalGun : Gun 
{
	public GameObject bullet;
	public float delay = 0.2f;
	public float bulletSpeed = 3.0f;
	
	private float currentDelay = 0.0f;
	
	void Update () 
	{
		if(Input.GetButton("Fire1") && currentDelay < 0.0f)
		{
			ammo--;
			currentDelay = delay;
			
			GameObject b = Instantiate(bullet) as GameObject;
			b.transform.position = this.transform.position;
			b.rigidbody.velocity = this.transform.forward * bulletSpeed;
		}
		else
		{
			currentDelay -= Time.deltaTime;
		}

		if(ammo < 1)
		{
			SendMessageUpwards("SwitchGun", true);
		}
	}
}
