﻿using UnityEngine;
using System.Collections;

public class GrenadeBullet : MonoBehaviour 
{
	public float lifetime;
	public GameObject explosion;
	
	void Update()	
	{
		lifetime -= Time.deltaTime;
		if(lifetime < 0)
		{
			Instantiate(explosion, this.transform.position, this.transform.rotation);
			Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Player" || collider.tag == "Weapon")
		{
			return;
		}
		else if(collider.tag == "Balloon")
		{
			Instantiate(explosion, this.transform.position, this.transform.rotation);
			Destroy (this.gameObject);
		}
	}
}
