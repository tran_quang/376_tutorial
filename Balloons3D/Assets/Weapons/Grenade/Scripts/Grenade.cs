﻿using UnityEngine;
using System.Collections;

public class Grenade : Gun 
{
	public GameObject bullet;
	public float throwForce = 20.0f;

	private float power = 0.0f;

	void Update () 
	{
		if(Input.GetButton("Fire1"))
		{
			power += throwForce * Time.deltaTime;
		}
		else if(Input.GetButtonUp ("Fire1"))
		{
			ammo--;

			GameObject b = Instantiate(bullet) as GameObject;
			b.transform.position = this.transform.position;
			b.rigidbody.velocity = Quaternion.AngleAxis(30, -this.transform.right) * this.transform.forward * power;

			power = 0;
		}
		
		if(ammo < 1)
		{
			SendMessageUpwards("SwitchGun", true);
		}
	}
}
