﻿using UnityEngine;
using System.Collections;

public class CrateFactory : MonoBehaviour 
{
	public static CrateFactory instance;
	public GameObject weaponCrate;

	private void Awake () 
	{
		if(instance != null)
		{
			Destroy (this.gameObject);
		}
		instance = this;
	}

	private void OnDestroy()
	{
		if(instance == this)
		{
			instance = null;
		}
	}

	public GameObject SpawnCrate(Vector3 position)
	{
		GameObject crate = Instantiate(weaponCrate, position, Quaternion.identity) as GameObject;
		return crate;
	}
}
