﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Crate : MonoBehaviour 
{
	public float pendulumForce;

	private void Awake()
	{
		rigidbody.AddForce(Vector3.right * pendulumForce);
	}

	private void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Player")
		{
			collider.gameObject.SendMessage("AddGun");
			Destroy (this.transform.parent.gameObject);
		}
	}
}
