﻿using UnityEngine;
using System.Collections;

public class DieAfterLifetime : MonoBehaviour 
{
	void Update () 
	{
		if(!this.particleSystem.IsAlive())
		{
			Transform t = this.transform;
			while(t.parent != null)
			{
				t = t.parent;
			}
			Destroy (t.gameObject);
		}
	}
}
