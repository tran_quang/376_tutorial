﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Life : MonoBehaviour 
{
	public int lifeRemaining;
	public float invincibilityTimer;
	public List<string> appliesDamage = new List<string>();
	public GameObject deathSystem;

	protected bool invincible = false;

	protected void OnTriggerEnter(Collider col)
	{
		if(appliesDamage.Contains(col.tag) && !invincible)
		{
			lifeRemaining--;
			if(lifeRemaining < 1)
			{
				Die();
			}
			else
			{
				StartCoroutine(TriggerInvincibility());
			}
		}
	}

	protected virtual IEnumerator TriggerInvincibility()
	{
		invincible = true;

		float waitFor = 0.2f;
		float timer = invincibilityTimer;
		bool visible = true;
		while(timer > 0)
		{
			visible = !visible;
			timer -= waitFor;
			yield return new WaitForSeconds(waitFor);
			ToggleAllRenderers(this.transform, visible);
		}

		ToggleAllRenderers(this.transform, true);
		invincible = false;
	}

	protected void ToggleAllRenderers(Transform t, bool enabled)
	{
		if(t.renderer != null)
		{
			t.renderer.enabled = enabled;
		}
		for(int i = 0; i < t.childCount; i++)
		{
			ToggleAllRenderers(t.GetChild(i), enabled);
		}
	}

	protected virtual void Die()
	{
		Instantiate(deathSystem, this.transform.position, this.transform.rotation);
		Destroy (this.gameObject);
	}
}
