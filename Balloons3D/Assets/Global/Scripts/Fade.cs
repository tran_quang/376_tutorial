﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour 
{
	public static Fade instance;

	private float alpha;
	private Texture2D texture;
	private string text;

	void Awake () 
	{
		if(instance != null)
		{
			Destroy (this.gameObject);
			return;
		}
		instance = this;
		this.enabled = false;

		texture = new Texture2D(1, 1);
		alpha = 0;
		texture.SetPixel (0, 0, new Color(0, 0, 0, alpha));
		texture.Apply ();
	}
	
	public void ToBlack(string text)
	{
		this.enabled = true;
		this.text = text;
	}

	public void Update()
	{
		if(alpha < 1)
		{
			alpha += Time.deltaTime;
		}
	}

	public void OnGUI()
	{
		texture.SetPixel(0, 0, new Color(0, 0, 0, alpha));
		texture.Apply ();
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);

		GUIStyle style = new GUIStyle();
		style.fontSize = 48;
		style.alignment = TextAnchor.MiddleCenter;
		style.normal.textColor = Color.black;
		GUI.Label (new Rect(Screen.width/2 + 1, Screen.height/2, 0, 0), text, style);
		GUI.Label (new Rect(Screen.width/2 - 1, Screen.height/2, 0, 0), text, style);
		GUI.Label (new Rect(Screen.width/2 + 1, Screen.height/2 + 1, 0, 0), text, style);
		GUI.Label (new Rect(Screen.width/2 - 1, Screen.height/2 + 1, 0, 0), text, style);
		GUI.Label (new Rect(Screen.width/2 + 1, Screen.height/2 - 1, 0, 0), text, style);
		GUI.Label (new Rect(Screen.width/2 - 1, Screen.height/2 - 1, 0, 0), text, style);
		GUI.Label (new Rect(Screen.width/2, Screen.height/2 + 1, 0, 0), text, style);
		GUI.Label (new Rect(Screen.width/2, Screen.height/2 - 1, 0, 0), text, style);

		style.normal.textColor = Color.white;
		GUI.Label (new Rect(Screen.width/2, Screen.height/2, 0, 0), text, style);
	}
}
