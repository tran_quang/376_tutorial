﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{
	public static GameController instance;

	public int numClusters;
	public int minBalloonsPerCluster;
	public int maxBalloonsPerCluster;
	public Vector3 worldLimits;

	public int score = 0;

	private int balloonCount = 0;
	private int totalBalloons = 0;

	private bool gameEnded = false;

	private void Awake()
	{
		if(instance != null)
		{
			Destroy (this.gameObject);
		}
		instance = this;
	}

	private void OnDestroy()
	{
		if(instance == this)
		{
			instance = null;
		}
	}

	private void Start () 
	{
		NewGame ();
	}

	private void Update()
	{
		if(gameEnded && Input.anyKeyDown)
		{
			Application.LoadLevel ("Menu");
		}
		if(Input.GetMouseButtonDown(0))
		{
			Screen.lockCursor = true;
		}
	}

	private void NewGame()
	{
		for(int i = 0; i < numClusters; i++)
		{
			float x = UnityEngine.Random.Range (-worldLimits.x, worldLimits.x);
			float y = UnityEngine.Random.Range (-worldLimits.y, worldLimits.y);
			float z = UnityEngine.Random.Range (-worldLimits.z, worldLimits.z);

			GameObject cluster = BalloonFactory.instance.SpawnCluster();
			cluster.transform.position = new Vector3(x, y, z);

			int numBalloons = UnityEngine.Random.Range(minBalloonsPerCluster, maxBalloonsPerCluster);
			for(int j = 0; j < numBalloons; j++)
			{
				GameObject balloon = BalloonFactory.instance.SpawnBalloon();
				Vector3 balloonPosition = cluster.transform.position;
				balloonPosition.x += UnityEngine.Random.Range (-1.0f, 1.0f);
				balloonPosition.y += 1.5f;
				balloonPosition.z += UnityEngine.Random.Range (-1.0f, 1.0f);
				balloon.transform.position = balloonPosition;


				Balloon component = balloon.GetComponent<Balloon>();
				cluster.SendMessage("AddBalloon", component);
				component.ChangeCluster(cluster);
				balloonCount++;
			}
		}

		totalBalloons = balloonCount;
	}

	public void PopBalloon()
	{
		balloonCount--;
		if(balloonCount == Mathf.CeilToInt(totalBalloons * 0.2f))
		{
			GameObject[] balloons = GameObject.FindGameObjectsWithTag("Balloon");
			for(int i = 0; i < balloons.Length; i++)
			{
				if(balloons[i].rigidbody != null)
				{
					balloons[i].rigidbody.velocity *= 2;
				}
			}
		}
		else if(balloonCount == Mathf.CeilToInt(totalBalloons * 0.1f) || 
		        balloonCount == Mathf.CeilToInt(totalBalloons * 0.4f) ||
		        balloonCount == Mathf.CeilToInt(totalBalloons * 0.7f))
		{
			GameObject hab = BalloonFactory.instance.SpawnHotAirBalloon();

			float x = UnityEngine.Random.Range (-worldLimits.x, worldLimits.x);
			float y = UnityEngine.Random.Range (-worldLimits.y, worldLimits.y);
			float z = UnityEngine.Random.Range (-worldLimits.z, worldLimits.z);
			hab.transform.position = new Vector3(x, y, z);
		}
		else if(balloonCount == 0)
		{
			EndGame ();
		}
	}

	public void EndGame()
	{
		GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>().enabled = false;
		if(balloonCount == 0)
		{
			Audio.instance.PlayGameComplete();
			Fade.instance.ToBlack("You Won!");
		}
		else
		{
			Audio.instance.PlayGameOver();
			Fade.instance.ToBlack("You Lost.");
		}
		StartCoroutine(EnableKeyPress());
	}

	private IEnumerator EnableKeyPress()
	{
		yield return new WaitForSeconds(3.0f);
		gameEnded = true;
	}
}
