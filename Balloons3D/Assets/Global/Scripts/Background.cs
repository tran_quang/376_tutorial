﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour 
{
	public string title;
	public string inputPrompt;
	private GUIStyle style;

	private float currentAlpha = 0;
	private bool increasing = true;

	void Awake()
	{
		style = new GUIStyle();
	}

	void Update()
	{
		if(increasing)
		{
			currentAlpha += Time.deltaTime;
			if(currentAlpha > 1)
			{
				increasing = false;
			}
		}
		else
		{
			currentAlpha -= Time.deltaTime;
			if(currentAlpha < 0)
			{
				increasing = true;
			}
		}
		if(Input.anyKeyDown)
		{
			Application.LoadLevel ("Main");
		}
	}

	void OnGUI () 
	{
		style.normal.textColor = Color.black;
		style.fontSize = 36;
		style.alignment = TextAnchor.UpperCenter;
		GUI.Label (new Rect(Screen.width/2, -1, 0, 0), title, style);
		GUI.Label (new Rect(Screen.width/2, 1, 0, 0), title, style);
		GUI.Label (new Rect(Screen.width/2 - 1, 0, 0, 0), title, style);
		GUI.Label (new Rect(Screen.width/2 + 1, 0, 0, 0), title, style);
		GUI.Label (new Rect(Screen.width/2 + 1, -1, 0, 0), title, style);
		GUI.Label (new Rect(Screen.width/2 + 1, 1, 0, 0), title, style);
		GUI.Label (new Rect(Screen.width/2 - 1, -1, 0, 0), title, style);
		GUI.Label (new Rect(Screen.width/2 - 1, 1, 0, 0), title, style);

		style.normal.textColor = new Color(0, 0, 0, currentAlpha);
		style.fontSize = 14;
		style.alignment = TextAnchor.LowerCenter;
		GUI.Label (new Rect(Screen.width/2, Screen.height - 1, 0, 0), inputPrompt, style);
		GUI.Label (new Rect(Screen.width/2, Screen.height + 1, 0, 0), inputPrompt, style);
		GUI.Label (new Rect(Screen.width/2 - 1, Screen.height, 0, 0), inputPrompt, style);
		GUI.Label (new Rect(Screen.width/2 + 1, Screen.height, 0, 0), inputPrompt, style);
		GUI.Label (new Rect(Screen.width/2 + 1, Screen.height - 1, 0, 0), inputPrompt, style);
		GUI.Label (new Rect(Screen.width/2 + 1, Screen.height + 1, 0, 0), inputPrompt, style);
		GUI.Label (new Rect(Screen.width/2 - 1, Screen.height - 1, 0, 0), inputPrompt, style);
		GUI.Label (new Rect(Screen.width/2 - 1, Screen.height + 1, 0, 0), inputPrompt, style);

		style.normal.textColor = Color.white;
		style.fontSize = 36;
		style.alignment = TextAnchor.UpperCenter;
		GUI.Label (new Rect(Screen.width/2, 0, 0, 0), title, style);

		style.normal.textColor = new Color(1, 1, 1, currentAlpha);
		style.fontSize = 14;
		style.alignment = TextAnchor.LowerCenter;
		GUI.Label (new Rect(Screen.width/2, Screen.height, 0, 0), inputPrompt, style);
	}
}
