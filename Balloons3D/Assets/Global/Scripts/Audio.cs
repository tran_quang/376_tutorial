﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Audio : MonoBehaviour 
{
	public static Audio instance;

	public AudioClip gameOverSound;
	public AudioClip gameCompleteSound;

	private AudioSource source;

	void Awake () 
	{
		if(instance != null)
		{
			Destroy (this.gameObject);
			return;
		}

		instance = this;
		source = GetComponent<AudioSource>();
	}

	void OnDestroy()
	{
		if(instance == this)
		{
			instance = null;
		}
	}

	public void PlayGameOver()
	{
		source.Stop ();
		source.PlayOneShot(gameOverSound);
	}

	public void PlayGameComplete()
	{
		source.Stop ();
		source.PlayOneShot(gameCompleteSound);
	}
}
