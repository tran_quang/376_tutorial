﻿using UnityEngine;
using System.Collections;

public class Wrap : MonoBehaviour 
{
	private Vector3 limits;

	void Start ()
	{
		limits = GameController.instance.worldLimits;
	}

	void Update () 
	{
		float buffer = 0.1f;
		Vector3 position = this.transform.position;
		if(Mathf.Abs (position.x) > limits.x)
		{
			if(position.x > 0)
			{
				position.x = -position.x + buffer;
			}
			else
			{
				position.x = -position.x - buffer;
			}
		}
		if(Mathf.Abs (position.y) > limits.y)
		{
			if(position.y > 0)
			{
				position.y = -position.y + buffer;
			}
			else
			{
				position.y = -position.y - buffer;
			}
		}
		if(Mathf.Abs (position.z) > limits.z)
		{
			if(position.z > 0)
			{
				position.z = -position.z + buffer;
			}
			else
			{
				position.z = -position.z - buffer;
			}
		}
		this.transform.position = position;
	}
}
