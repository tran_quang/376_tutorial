﻿using UnityEngine;
using System.Collections;

public class Bounce : MonoBehaviour 
{
	public float explosiveForce;

	private Vector3 limits;

	void Start () 
	{
		limits = GameController.instance.worldLimits;
	}
	
	void Update () 
	{
		bool explode = false;
		Vector3 position = this.transform.position;
		if(Mathf.Abs (position.x) > limits.x)
		{
			explode = true;
			if(position.x > 0)
			{
				position.x += 1;
 			}
			else
			{
				position.x -= 1;
			}
		}
		if(Mathf.Abs (position.y) > limits.y)
		{
			explode = true;
			if(position.y > 0)
			{
				position.y += 1;
			}
			else
			{
				position.y -= 1;
			}
		}
		if(Mathf.Abs (position.z) > limits.z)
		{
			explode = true;
			if(position.z > 0)
			{
				position.z += 1;
			}
			else
			{
				position.z -= 1;
			}
		}
		if(explode)
		{
			rigidbody.AddExplosionForce (explosiveForce, position, 1);
		}
	}
}
