﻿using UnityEngine;
using System.Collections.Generic;

public class LifeCounter : MonoBehaviour, GUIListener
{
	private const float HEART_PADDING = 0.4f;

	public GameObject heart;

	private int index = 0;
	private List<GameObject> hearts = new List<GameObject>();

	private void Awake()
	{
		Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
        position.x += heart.renderer.bounds.extents.x + HEART_PADDING/2;
        position.y -= heart.renderer.bounds.extents.y + HEART_PADDING/2;
		position.z = 0;

		this.transform.position = position;

		// Listen to the player's life
		Life life = GameObject.FindGameObjectWithTag("Player").GetComponent<Life>();
		life.RegisterListener(this);
		life.numLives = Player.NUM_LIVES;
		Init (life.numLives);
	}

	public void Init(int numLives)
	{
		Vector3 position = Vector3.zero;
		for(int i = 0; i < numLives; i++)
		{
			GameObject h = Instantiate (heart) as GameObject;
			hearts.Add (h);

			position.x = (heart.renderer.bounds.max.x + HEART_PADDING) * i;

			h.transform.parent = this.transform;
			h.transform.localPosition = position;
		}

		index = numLives - 1;
	}

	public void Notify(Life life)
	{
		//Offset index by 1 to get number of lives
		if(life.numLives < index + 1)
		{
			hearts[index--].SetActive(false);
		}
		else if(life.numLives > index + 1)
		{
			hearts[index++].SetActive(true);
		}
	}
}
