﻿using UnityEngine;
using System.Collections;

public class ButtonPlayAgain : MonoBehaviour 
{
    private SpriteRenderer spriteRenderer;
    private Color original;

    private void Awake()
    {
        spriteRenderer = this.renderer as SpriteRenderer;
        original = spriteRenderer.color;
    }

    private void OnMouseDown()
    {
        Color c = spriteRenderer.color;
        c.a *= 0.5f;
        spriteRenderer.color = c;
    }

    private void OnMouseUp()
    {
        spriteRenderer.color = original;
    }

    private void OnMouseUpAsButton()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
