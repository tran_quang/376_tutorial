using System.Collections.Generic;

public interface GUIListener
{
	void Notify(Life life);
}
