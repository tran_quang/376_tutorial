﻿using UnityEngine;
using System.Collections;

public class ScoreDisplay : MonoBehaviour 
{
    private const int OFFSET = 2;

	public int Score {get; set;}

	private GUIStyle style;

	private void Awake()
	{
		style = new GUIStyle();
		style.fontSize = 38;
	}

	private void OnGUI()
	{
        for(int i = 0; i < 5;i ++)
        {
            style.normal.textColor = GetColor(i);
            GUI.Label(GetRect(i), Score.ToString (), style);
        }
	}

    private Rect GetRect(int index)
    {
        switch(index)
        {
            case 0: return new Rect(Screen.width/2 + OFFSET, 20, 100, 100);
            case 1: return new Rect(Screen.width/2 - OFFSET, 20, 100, 100);
            case 2: return new Rect(Screen.width/2, 20 + OFFSET, 100, 100);
            case 3: return new Rect(Screen.width/2, 20 - OFFSET, 100, 100);
            case 4: return new Rect(Screen.width/2, 20, 100, 100);
            default: return new Rect(0, 0, 0, 0);
        }
    }

    private Color GetColor(int index)
    {
        switch(index)
        {
            case 4: return new Color(1, 1, 1);
            default: return new Color(0, 0, 0);
        }
    }
}
