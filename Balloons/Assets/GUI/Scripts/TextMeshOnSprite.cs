﻿using UnityEngine;
using System.Collections;

public class TextMeshOnSprite : MonoBehaviour 
{
	private void Awake () 
    {
        var parentSpriteRenderer = this.transform.parent.GetComponent<SpriteRenderer>();
        if( parentSpriteRenderer != null )
        {
            this.renderer.sortingLayerID = this.transform.parent.GetComponent<SpriteRenderer>().sortingLayerID;
            this.renderer.sortingOrder = this.transform.parent.GetComponent<SpriteRenderer>().sortingOrder + 10;
        }
	}
}
