﻿using UnityEngine;
using System.Collections;

public class Dart : MonoBehaviour 
{
	private const float BULLET_SPEED = 10.0f;

	private float lifetime = 0.0f;

	private void Update()
	{
		lifetime -= Time.deltaTime;
		if(lifetime < 0)
		{
			this.gameObject.SetActive(false);
		}
	}

	private void OnTriggerEnter2D(Collider2D collider)
	{
	}

	private void Shoot()
	{
		rigidbody2D.velocity = (transform.up * BULLET_SPEED);
		lifetime = 1.0f;
	}
}
