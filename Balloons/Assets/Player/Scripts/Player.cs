﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
	public static int NUM_LIVES = 3;

	public GameObject cluster;

	private const float ROTATION_SPEED = 200.0f;
	private const float MOVE_SPEED = 300.0f;
	private const float INVINCIBILITY_TIMER = 2.5f;
	private const float BLINK_PERIOD = 0.25f;

	private float dartCooldown = 0.0f;
	private Transform gunOffset;
	private Life life;
	private SpriteRenderer spriteRenderer;
	private Animator animator;

	void Start () 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		gunOffset = transform.FindChild("Gun").transform;
		animator = GetComponent<Animator>();
		life = GetComponent<Life>();

		Physics2D.IgnoreLayerCollision(cluster.layer, this.gameObject.layer);
		StartCoroutine(TriggerInvincibility(INVINCIBILITY_TIMER, BLINK_PERIOD));
	}
	
	void Update () 
	{
		float horizontal = -Input.GetAxis ("Horizontal");
		rigidbody2D.angularVelocity = ROTATION_SPEED * horizontal;

		float vertical = Input.GetAxis ("Vertical");
		rigidbody2D.AddForce(transform.up * vertical * MOVE_SPEED * Time.deltaTime);

		if(dartCooldown > 0.0f)
		{
			dartCooldown -= Time.deltaTime;
		}
		else if(Input.GetButton ("Fire1"))
		{
			GameObject dart = Pools.GetPool(Pools.DARTS).GetObject ();
			dart.transform.position = gunOffset.transform.position;
			dart.transform.rotation = this.transform.rotation;

            dart.SendMessage("Shoot");
			dartCooldown = 0.2f;
		}

		if(Input.GetKeyDown(KeyCode.I))
		{
			this.collider2D.enabled = !this.collider2D.enabled;
		}
	}

	private void HitByBalloon()
	{
		Physics2D.IgnoreLayerCollision(cluster.layer, this.gameObject.layer);
		rigidbody2D.angularVelocity = 0;
		rigidbody2D.velocity = Vector2.zero;
		animator.SetTrigger ("Die");
		life.Die ();
		this.enabled = false;
	}

	private void NewLife()
	{
		if(life.numLives > 0)
		{
			ResetPosition();
			this.enabled = true;
			StartCoroutine(TriggerInvincibility(INVINCIBILITY_TIMER, BLINK_PERIOD));
		}
		else
		{
			GameOver();
		}
	}

	private void ResetPosition()
	{
		Vector3 center = new Vector3(Camera.main.pixelWidth/2, Camera.main.pixelHeight/2, 0);
		center = Camera.main.ScreenToWorldPoint(center);
		center.z = 0;
		this.transform.position = center;
		this.transform.rotation = Quaternion.identity;
	}

	private IEnumerator TriggerInvincibility(float duration, float timeBetweenBlinks)
	{
		while(duration > 0.0f)
		{
			duration -= timeBetweenBlinks;
			spriteRenderer.enabled = !spriteRenderer.enabled;

			for(int i = 0; i < transform.childCount; i++)
			{
				Transform child = transform.GetChild (i);
				if(child.renderer != null)
				{
					child.renderer.enabled = !child.renderer.enabled;
				}
			}

			yield return new WaitForSeconds(timeBetweenBlinks);
		}

		spriteRenderer.enabled = true;
		for(int i = 0; i < transform.childCount; i++)
		{
			Transform child = transform.GetChild (i);
			if (child.renderer != null) 
			{
				child.renderer.enabled = true;
			}
		}
		Physics2D.IgnoreLayerCollision(cluster.layer, this.gameObject.layer, false);
	}

	private void GameOver()
	{
		for(int i = 0; i < transform.childCount; i++)
		{
			Transform child = transform.GetChild (i);
			if (child.renderer != null) 
			{
				child.renderer.enabled = false;
            }
		}
		this.renderer.enabled = false;
		Physics2D.IgnoreLayerCollision(cluster.layer, this.gameObject.layer);

        GameObject.Find("GameOver").SendMessage("EndGame", "You lost!\nTry again next time!");
	}
}
