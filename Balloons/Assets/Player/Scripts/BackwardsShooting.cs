﻿using UnityEngine;
using System.Collections;

public class BackwardsShooting : MonoBehaviour 
{
    public float timer = 3.0f;

    private bool started = false;
    private float cooldown = 0.0f;
    private Transform gunOffset;

    private void Awake()
    {
        gunOffset = this.transform.FindChild("Gun");
    }

	// Update is called once per frame
	void Update () 
    {
	    if(Input.GetKeyDown(KeyCode.V))
        {
            started = true;
        }

        if(started)
        {
            timer -= Time.deltaTime;
            if(cooldown > 0.0f)
            {
                cooldown -= Time.deltaTime;
            }
            else if(Input.GetButton ("Fire2"))
            {
                GameObject dart = Pools.GetPool (Pools.DARTS).GetObject ();
                dart.transform.position = gunOffset.transform.position;
                
                Vector3 rotation = this.transform.rotation.eulerAngles;
                rotation.z += 180.0f;
                dart.transform.rotation = Quaternion.Euler (rotation);
                
                dart.SendMessage("Shoot");
                cooldown = 0.2f;
            }

            if(timer < 0)
            {
                Destroy(this);
            }
        }
	}
}
