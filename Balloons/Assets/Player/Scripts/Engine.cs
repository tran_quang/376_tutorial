﻿using UnityEngine;
using System.Collections;

public class Engine : MonoBehaviour 
{
	void Awake () 
	{
		SpriteRenderer parentRenderer = this.transform.parent.GetComponent<SpriteRenderer>();
		this.renderer.sortingLayerID = parentRenderer.sortingLayerID;
		this.renderer.sortingOrder = parentRenderer.sortingLayerID - 1;
	}
}
