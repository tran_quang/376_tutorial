﻿using UnityEngine;
using System.Collections.Generic;

public class Life : MonoBehaviour 
{
	public int numLives = 3;
	private List<GUIListener> listeners = new List<GUIListener>();

	public void Die()
	{
		numLives--;
		for(int i = 0; i < listeners.Count; i++)
		{
			listeners[i].Notify(this);
		}
	}

	public bool AnyLivesLeft()
	{
		return numLives > 0;
	}

	public void RegisterListener(GUIListener l)
	{
		listeners.Add (l);
	}
}
