using UnityEngine;
using System.Collections;

public class Pumpkin : HotAirBalloon
{
	private const float PROJECTILE_SPEED = 10.0f;
	private Pool candies;

	protected override void Awake ()
	{
		base.Awake ();
		life.numLives = 20;
		candies = Pools.GetPool(Pools.CANDIES);
	}

	protected override void Shoot ()
	{
		Vector3 direction = player.position - this.transform.position;
		direction.Normalize();

		for(int i = 0; i < 3; i++)
		{
			GameObject go = candies.GetObject();
			go.transform.position = this.transform.position;

			if(i == 0)
			{
				direction.x -= 0.25f;
			}
			else
			{
				direction.x += 0.25f;
			}
			go.transform.up = direction;

			Vector2 velocity = direction * PROJECTILE_SPEED;
			go.SendMessage ("Shoot", velocity);
		}
	}
}
