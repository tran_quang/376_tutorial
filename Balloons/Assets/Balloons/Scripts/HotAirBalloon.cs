﻿using UnityEngine;
using System.Collections;

public class HotAirBalloon : MonoBehaviour 
{
	public static int NUM_LIVES = 5;

	private const float BALLOON_COOLDOWN = 3.0f;
	private const float PROJECTILE_SPEED = 8.0f;

	private Pool waterBalloons;
	private float balloonCooldown = 3.0f;
	private CircleCollider2D circleCollider;
	protected Transform player;
	protected Life life;

	protected virtual void Awake ()
	{
		StartCoroutine(SpawnWarning(2.0f, 0.25f));
		player = GameObject.FindGameObjectWithTag("Player").transform;
		circleCollider = GetComponent<CircleCollider2D>();
		life = GetComponent<Life>();
		life.numLives = NUM_LIVES;
		circleCollider.enabled = false;
		this.enabled = false;
	}

	void Update () 
	{
		balloonCooldown -= Time.deltaTime;
		if(balloonCooldown < 0)
		{
			balloonCooldown = BALLOON_COOLDOWN;
			Shoot ();
		}
	}

	private void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.tag == "Dart")
		{
			life.Die ();
			if(life.numLives == 0)
			{
				GameObject go = Pools.GetPool(Pools.LARGE_POP_EFFECT).GetObject();
				go.transform.position = this.transform.position;
				go.particleSystem.Play();

				GameController.instance.AddScore(10);
				GameController.instance.PopHotAirBalloon();

				Destroy (this.gameObject);
			}
			collider.gameObject.SetActive(false);
		}
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
	}

	protected virtual void Shoot ()
	{
		GameObject go = waterBalloons.GetObject();
		go.transform.position = this.transform.position;

		Vector3 direction = player.position - this.transform.position;
		direction.Normalize();
		go.transform.up = -direction;

		Vector2 velocity = direction * PROJECTILE_SPEED;
		go.SendMessage ("Shoot", velocity);
	}

	private IEnumerator SpawnWarning(float duration, float blinkTime)
	{
		while(duration > 0)
		{
			duration -= blinkTime;
			this.renderer.enabled = !this.renderer.enabled;
			yield return new WaitForSeconds(blinkTime);
		}

		circleCollider.enabled = true;
		this.renderer.enabled = true;
		this.enabled = true;

		waterBalloons = Pools.GetPool(Pools.WATER_BALLOONS);

		float x = UnityEngine.Random.Range(-2.0f, 2.0f);
		this.rigidbody2D.velocity = new Vector2(x, 0);
	}
}
