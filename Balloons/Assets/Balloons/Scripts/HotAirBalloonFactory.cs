﻿using UnityEngine;
using System.Collections;

public class HotAirBalloonFactory : MonoBehaviour 
{
	public static HotAirBalloonFactory instance;

	public GameObject hotAirBalloon;

	void Awake () 
	{
		instance = this;
	}

	public void CreateHotAirBalloon()
	{
		float x = UnityEngine.Random.Range(0, Screen.width);
		float y = UnityEngine.Random.Range(0, Screen.height);
		
		Vector3 position = new Vector3(x, y, 0);
		position = Camera.main.ScreenToWorldPoint(position);
		position.z = 0;
		
		Instantiate(hotAirBalloon, position, Quaternion.identity);
	}
}
