﻿using UnityEngine;
using System.Collections.Generic;

public class Cluster : MonoBehaviour 
{
	public static float SpeedMultiplier {get; set;}

	// The max speed and multiplier a cluster will be slowed by when it reaches the thresholds
	private const float MAX_SPEED = 4.0f;
	private const float CLUSTER_SLOW = 0.5f;

	// Slow down the cluster when it reaches a certain number of balloons
	private const int LOW_SPEED_THRESHOLD = 5;
	private const int MED_SPEED_THRESHOLD = 2;


	// Maximum distances balloons of the same cluster can be
	public static float MAX_BALLOON_DISTANCE = 0.5f;

	// All balloons in the same cluster
    private List<GameObject> balloons = new List<GameObject>();

	void Awake () 
    {
		float x = UnityEngine.Random.Range(-MAX_SPEED, MAX_SPEED) * SpeedMultiplier;
		float y = UnityEngine.Random.Range(-MAX_SPEED, MAX_SPEED) * SpeedMultiplier;
        rigidbody2D.velocity = new Vector2(x, y);
	}

	private void Collide(GameObject go)
	{
		bool isPlayer = false;
		if(go.tag == "Dart")
		{
			go.SetActive (false);
		}
		else if(go.tag == "Player")
		{
			isPlayer = true;
            go.SendMessage("HitByBalloon");
		}

		if(balloons.Count > 1)
		{
			Split ();
			if(!isPlayer)
			{
				GameController.instance.AddScore(1);
			}
		}
		else
		{
			GameController.instance.PopBalloon();
			if(!isPlayer)
			{
				GameController.instance.AddScore(2);
			}

			Vector3 position = this.transform.GetChild(0).position; // Only 1 child since it is a cluster of 1 balloon
			GameObject pop = Pools.GetPool(Pools.POP_EFFECT).GetObject();
			pop.transform.position = position;
			pop.particleSystem.Play ();
		}
		Destroy (this.gameObject);
	}

    private void Split()
    {
        // Randomly decide how many balloons go in each cluster (but cluster must have at least 1 balloon)
        int split = UnityEngine.Random.Range(1, balloons.Count - 1);

        GameObject newCluster = BalloonFactory.instance.GetCluster();
		newCluster.transform.position = this.transform.position;
		newCluster.transform.parent = this.transform.parent;

        List<GameObject> newClusterBalloons = balloons.GetRange(0, split);
        for(int i = 0; i < newClusterBalloons.Count; i++)
        {
            newClusterBalloons[i].transform.parent = newCluster.transform;
			newCluster.SendMessage ("AddBalloon", newClusterBalloons[i]);
        }

        newCluster = BalloonFactory.instance.GetCluster ();
		newCluster.transform.position = this.transform.position;
		newCluster.transform.parent = this.transform.parent;

        newClusterBalloons = balloons.GetRange(split, balloons.Count - split);
        for(int i = 0; i < newClusterBalloons.Count; i++)
        {
            newClusterBalloons[i].transform.parent = newCluster.transform;
			newCluster.SendMessage ("AddBalloon", newClusterBalloons[i]);
        }
    }

    public void AddBalloon(GameObject balloon)
    {
        if(balloon.tag != "Balloon")
        {
            return;
        }

        balloons.Add(balloon);
		if(balloons.Count == MED_SPEED_THRESHOLD || balloons.Count == LOW_SPEED_THRESHOLD)
		{
			rigidbody2D.velocity *= CLUSTER_SLOW;
		}
    }

	public void SpeedUp()
	{
		rigidbody2D.velocity *= Cluster.SpeedMultiplier;
	}
}
