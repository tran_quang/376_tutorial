﻿using UnityEngine;
using System.Collections;

public class BalloonFactory : MonoBehaviour 
{
	public static BalloonFactory instance;

    public GameObject balloon;
    public GameObject balloonCluster;
    public int minBalloonsPerCluster;
    public int maxBalloonsPerCluster;
    public int numClusters;

	void Start () 
    {
		CreateClusters();
		instance = this;
	}

	public GameObject GetCluster()
	{
		return Instantiate (balloonCluster) as GameObject;
	}

	public void CreateClusters()
	{
		for(int i = 0; i < numClusters; i++)
		{
			float x = UnityEngine.Random.Range(0, Screen.width);
			float y = UnityEngine.Random.Range(0, Screen.height);
			
			Vector3 position = new Vector3(x, y, 0);
			position = Camera.main.ScreenToWorldPoint(position);
			position.z = 0;
			
			GameObject c = Instantiate(balloonCluster, position, Quaternion.identity) as GameObject;
			c.transform.parent = this.transform;
			
			int numBalloons = UnityEngine.Random.Range(minBalloonsPerCluster, maxBalloonsPerCluster + 1);
			for(int j = 0; j < numBalloons; j++)
			{
				GameObject b = Instantiate(balloon, position, Quaternion.identity) as GameObject;
				GameController.instance.AddBalloon();
				c.SendMessage("AddBalloon", b);
				
				b.transform.parent = c.transform;
				
				x = UnityEngine.Random.Range (-Cluster.MAX_BALLOON_DISTANCE, Cluster.MAX_BALLOON_DISTANCE);
				y = UnityEngine.Random.Range (-Cluster.MAX_BALLOON_DISTANCE, Cluster.MAX_BALLOON_DISTANCE);
				b.transform.localPosition = new Vector2(x, y);
			}
		}
	}
}
