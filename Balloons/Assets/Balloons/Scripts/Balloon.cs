﻿using UnityEngine;
using System.Collections;

public class Balloon : MonoBehaviour 
{
	void Awake () 
	{
		SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

		float r = UnityEngine.Random.Range (0, 1.0f);
		float g = UnityEngine.Random.Range (0, 1.0f);
		float b = UnityEngine.Random.Range (0, 1.0f);

		Color color = new Color(r,g,b);

		spriteRenderer.color = color;
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		SendMessageUpwards("Collide", collider.gameObject);
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		SendMessageUpwards("Collide", collision.collider.gameObject);
	}
}
