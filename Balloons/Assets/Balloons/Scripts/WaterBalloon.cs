﻿using UnityEngine;
using System.Collections;

public class WaterBalloon : MonoBehaviour 
{
	private float lifetime = 1.0f;

	protected virtual void Awake()
	{
		SpriteRenderer sr = this.renderer as SpriteRenderer;
		float r = UnityEngine.Random.Range (0.0f, 1.0f);
		float g = UnityEngine.Random.Range (0.0f, 1.0f);
		float b = UnityEngine.Random.Range (0.0f, 1.0f);
		sr.color = new Color(r, g, b);
	}

	private void Update()
	{
		lifetime -= Time.deltaTime;
		if(lifetime < 0)
		{
			this.gameObject.SetActive(false);
		}
	}

	public void Shoot(Vector2 velocity)
	{
		this.rigidbody2D.velocity = velocity;
		lifetime = 1.0f;
	}

    private void OnTriggerEnter2D(Collider2D collider)
    {
        GameObject effect = Pools.GetPool(Pools.WATER_BALLOON_POP_EFFECT).GetObject();
        effect.transform.position = this.transform.position;
        effect.particleSystem.Play();

        if(collider.tag == "Player")
        {
            collider.gameObject.SendMessage("HitByBalloon");
        }
        else if(collider.tag == "Dart")
        {
            collider.gameObject.SetActive(false);
        }
        this.gameObject.SetActive(false);
    }
}
