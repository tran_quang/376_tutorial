﻿using UnityEngine;
using System.Collections;

public class ParticleSystemInactiveOnDeath : MonoBehaviour 
{
	private void Update()
	{
        if(!particleSystem.IsAlive())
        {
            this.gameObject.SetActive(false);
        }
	}
}
