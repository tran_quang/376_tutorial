﻿using UnityEngine;
using System.Collections.Generic;

public class Pool : MonoBehaviour 
{
	public GameObject poolable;
	public int numObjects;
	public string label;

	private int index = 0;
	private List<GameObject> objects = new List<GameObject>();

	protected void Awake()
	{
		for(int i = 0; i < numObjects; i++)
		{
			GameObject o = Instantiate(poolable) as GameObject;
			o.SetActive (false);
			o.transform.parent = this.transform;
			objects.Add(o);
		}
	}
	
	public GameObject GetObject()
	{
		GameObject o = objects[index];
		o.SetActive(true);
		index = (index+1)%numObjects;
		return o;
	}
}
