﻿using UnityEngine;
using System.Collections;

public class Wrap : MonoBehaviour 
{
	private const float WRAP_BUFFER = 0.1f;

	Camera screenCamera;

	void Awake()
	{
		screenCamera = Camera.main;
	}

	void Update()
	{
		Vector3 position = this.transform.position;

		Vector3 screenPosition = screenCamera.WorldToScreenPoint(position);

		if(screenPosition.x < 0)
		{
			position.x = -position.x - WRAP_BUFFER;
		}
		else if(screenPosition.x > Screen.width)
		{
			position.x = -position.x + WRAP_BUFFER;
		}

		if(screenPosition.y < 0)
		{
			position.y = -position.y - WRAP_BUFFER;
		}
		else if(screenPosition.y > Screen.height)
		{
			position.y = -position.y + WRAP_BUFFER;
		}

		this.transform.position = position;
	}
}
