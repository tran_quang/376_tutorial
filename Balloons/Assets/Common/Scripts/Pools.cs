﻿using UnityEngine;
using System.Collections.Generic;

public class Pools : MonoBehaviour 
{
	public static string DARTS = "Darts";
	public static string WATER_BALLOONS = "WaterBalloons";
	public static string POP_EFFECT = "BalloonPop";
	public static string LARGE_POP_EFFECT = "LargeBalloonPop";    
	public static string WATER_BALLOON_POP_EFFECT = "WaterBalloonPop";
	public static string CANDIES = "Candies";

	private static Pools instance;

	private Dictionary<string, Pool> pools = new Dictionary<string, Pool>();

	void Awake () 
	{
		Pool[] p = GetComponentsInChildren<Pool>();
		for(int i = 0; i < p.Length; i++)
		{
			pools.Add (p[i].label, p[i]);
		}

		instance = this;
	}

	public static Pool GetPool(string label)
	{
		return instance.pools[label];
	}
}
