﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour 
{
	public float minVelocity;
	public float maxVelocity;

	public float minScale;
	public float maxScale;

	bool isGoingRight = true;

	void Awake()
	{
		float velocity = UnityEngine.Random.Range (minVelocity, maxVelocity + 1);
		int goingRight = UnityEngine.Random.Range (0, 2);

		if(goingRight == 0)
		{
			isGoingRight = false;
			velocity = -velocity;
		}
		rigidbody2D.velocity = Vector2.right * velocity;

		float scale = UnityEngine.Random.Range (minScale, maxScale);
		this.transform.localScale = Vector3.one * scale;
	}

	void OnBecameInvisible () 
	{
		if(Camera.main == null)
		{
			return;
		}

		Vector3 screenPosition = Camera.main.WorldToScreenPoint(this.transform.position);
		if(screenPosition.x < 0 || screenPosition.x > Screen.width)
		{
			screenPosition.y = UnityEngine.Random.Range (0, Screen.height);
			Vector3 position = Camera.main.ScreenToWorldPoint(screenPosition);
			position.x = -position.x;
			this.transform.position = position;

			float velocity = UnityEngine.Random.Range (minVelocity, maxVelocity + 1);
			if(!isGoingRight)
			{
				velocity = -velocity;
			}
			rigidbody2D.velocity = Vector2.right * velocity;

			float scale = UnityEngine.Random.Range (minScale, maxScale);
			this.transform.localScale = Vector3.one * scale;
		}
	}
}
