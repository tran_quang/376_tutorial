﻿using UnityEngine;
using System.Collections.Generic;

public class CloudBackground : MonoBehaviour 
{
	public GameObject cloud;
	public int minClouds;
	public int maxClouds;

	private List<GameObject> clouds = new List<GameObject>();

	void Awake () 
	{
		int numClouds = UnityEngine.Random.Range (minClouds, maxClouds + 1);
		for(int i = 0; i < numClouds; i++)
		{
			float x = UnityEngine.Random.Range(0, Screen.width);
			float y = UnityEngine.Random.Range(0, Screen.height);

			Vector3 position = new Vector3(x, y, 0);
			position = Camera.main.ScreenToWorldPoint(position);
			position.z = 0;

			GameObject c = Instantiate(cloud, position, Quaternion.identity) as GameObject;
			clouds.Add (c);

			c.transform.parent = this.transform;
		}
	}
}
