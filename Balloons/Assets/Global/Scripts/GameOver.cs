﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour 
{
    private TextMesh text;
    private Animator animator;

	private void Awake () 
    {
        text = this.transform.FindChild("DialogTextLabel").GetComponent<TextMesh>();    
        animator = GetComponent<Animator>();
	}
	
    public void EndGame(string displayText)
    {
        text.text = displayText;
        animator.SetTrigger("Popup");
    }
}
