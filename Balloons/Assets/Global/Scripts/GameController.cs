﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{
	public static GameController instance;


	// Speed up multiplier when 80% of balloons have been popped
	private const float EXTRA_SPEED = 2.0f;

	private int Score;
	private ScoreDisplay scoreDisplay;

	private int numBalloons;
	private int maxBalloons;
	private int numHotAirBalloons;

	private bool spawnedPumpkin;
	public GameObject pumpkin;

	void Awake () 
	{
		if(instance != null)
		{
			Destroy (this.gameObject);
		}
		instance = this;

		scoreDisplay = GameObject.Find ("ScoreDisplay").GetComponent<ScoreDisplay>();
		numBalloons = 0;
		Score = 0;

		Cluster.SpeedMultiplier = 1.0f;
	}

	public void AddScore(int addition)
	{
		Score += addition;
		scoreDisplay.Score = Score;
	}

	public void AddBalloon()
	{
		numBalloons++;
		maxBalloons++;
	}

	public void PopBalloon()
	{
		numBalloons--;
		if(numBalloons == Mathf.CeilToInt(maxBalloons * 0.2f))
		{
			Cluster.SpeedMultiplier = 2.0f;
			GameObject[] balloons = GameObject.FindGameObjectsWithTag("Balloon");
			for(int i = 0; i < balloons.Length; i++)
			{
				balloons[i].SendMessage ("SpeedUp", SendMessageOptions.DontRequireReceiver);
			}
		}
		else if(numBalloons == Mathf.CeilToInt(maxBalloons * 0.1f) || 
		        numBalloons == Mathf.CeilToInt(maxBalloons * 0.4f) ||
		        numBalloons == Mathf.CeilToInt(maxBalloons * 0.7f))
		{
			HotAirBalloonFactory.instance.CreateHotAirBalloon();
			numHotAirBalloons++;
		}
		else if(numBalloons == 0 && numHotAirBalloons == 0)
		{
            EndGame();
		}
	}

	public void PopHotAirBalloon()
	{
		numHotAirBalloons--;
		if(numBalloons == 0 && numHotAirBalloons == 0)
		{
            EndGame();
		}
	}

    private void EndGame()
    {
		if(!spawnedPumpkin)
		{
			numHotAirBalloons++;
			spawnedPumpkin = true;
			Instantiate (pumpkin, Vector3.zero, Quaternion.identity);
		}
		else
		{
			GameObject.Find("GameOver").SendMessage("EndGame", "You won!\nCongratulations!");
		}
    }
}
